#!/bin/bash
set -e

git submodule init
git submodule update --recursive --remote

cp main.conf .env

## BUILD_ROOT_PATH env
# This is a workarround for
# https://github.com/docker/compose/issues/7873
# See also BUILD_ROOT_PATH sed section at the end of file
echo "BUILD_ROOT_PATH=$(pwd)" >> .env

. ./.env

## Create LDAP based DN from domain
#DOMAIN_ARRAY="$DOMAIN"
#IFS=. DOMAINPARTS=(${DOMAIN_ARRAY##*-})
#DOMAIN_BASE_DN=''
#for PART in "${DOMAINPARTS[@]}" ; do
#    DOMAIN_BASE_DN=$DOMAIN_BASE_DN'dc='$PART','
#done
#DOMAIN_BASE_DN=$(echo $DOMAIN_BASE_DN | sed 's/,$//')
#echo "DOMAIN_BASE_DN=$DOMAIN_BASE_DN" >> .env

PARTS_PATH=docker-compose-parts
source main.conf
export $(cat main.conf | egrep -v "^\s*(#|$)" |cut -d= -f1)
envsubst < config/hydra/hydra.yml.template > config/hydra/hydra.yml
envsubst < config/mokey/mokey.yaml.template > config/mokey/mokey.yaml

cp .env docker-compose-parts/
docker-compose -f docker-compose-parts/haproxy.yml -f docker-compose-parts/freeipa.yml -f docker-compose-parts/lum.yml -f docker-compose-parts/mokey.yml -f docker-compose-parts/hydra.yml -f docker-compose-parts/network.yml config > docker-compose.yml 
